package com.example.demo;

public class ArticleChange {

    private Article oldArticle;

    private Article newArticle;

    public ArticleChange(final Integer oldArticleId, final Integer oldPrice, final Integer oldStock,
                         final Integer newArticleId, final Integer newPrice, final Integer newStock) {
        if (oldArticleId != null) {
            this.oldArticle = new Article(oldArticleId, oldPrice, oldStock);
        }
        if (newArticleId != null) {
            this.newArticle = new Article(newArticleId, newPrice, newStock);
        }
    }

    public boolean isNew() {
        return oldArticle == null;
    }

    public boolean isDeleted() {
        return newArticle == null;
    }

    public boolean hasChanged() {
        return isNew()
                || isDeleted()
                || (oldArticle.price != newArticle.price && oldArticle.stock != newArticle.stock);
    }

    @Override
    public String toString() {
        if (isNew()) {
            return String.format("Article %s: new(%d¤, %d)", newArticle.articleId, newArticle.price, newArticle.stock);
        }
        if (isDeleted()) {
            return String.format("Article %s: deleted", oldArticle.articleId);
        }
        return String.format("Article %s: changed(%d¤, %d)->(%d¤, %d)", newArticle.articleId, oldArticle.price, oldArticle.stock, newArticle.price, newArticle.stock);
    }

    private static class Article {

        private final int articleId;
        private final int price;
        private final int stock;

        private Article(final int articleId, final int price, final int stock) {
            this.articleId = articleId;
            this.price = price;
            this.stock = stock;
        }

    }

}
