package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.StopWatch;

@SpringBootApplication
@EnableTransactionManagement
public class DemoApplication implements CommandLineRunner {

    @Autowired
    ArticleService articleService;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(final String... args) {
        //Generate some example Data
        int count = 100_000;
        int versions = 10;

        for (int version = 1; version < versions; version++) {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start("createData");
            articleService.createData(count, version);
            stopWatch.stop();
            System.out.printf("Inserted Version %d in %.2fs%n",
                    version,
                    stopWatch.getLastTaskInfo().getTimeSeconds());


            stopWatch.start("processChanges");
            long changed = articleService.processChanges(version);
            stopWatch.stop();
            System.out.printf("Changed %d records from version %d to %d in %.2fs%n",
                    changed,
                    (version - 1),
                    version,
                    stopWatch.getLastTaskInfo().getTimeSeconds());


            System.out.printf("Total Time for Version %d: %.2fs%n",
                    version,
                    stopWatch.getTotalTimeSeconds());
        }

        System.exit(0);
    }
}
