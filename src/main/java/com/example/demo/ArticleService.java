package com.example.demo;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.jooq.DSLContext;
import org.jooq.util.maven.example.tables.records.ArticleRecord;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static org.jooq.util.maven.example.Tables.*;

@Service
public class ArticleService {

    private final Random random = new Random();

    public final DSLContext dsl;

    private final RabbitTemplate rabbitTemplate;

    private final Queue queue;

    public ArticleService(final RabbitTemplate rabbitTemplate, final Queue queue, final DSLContext dsl) {
        this.rabbitTemplate = rabbitTemplate;
        this.queue = queue;
        this.dsl = dsl;
    }

    /**
     * Creates (up to {@code amount}) new records.
     */
    @Transactional
    public void createData(int count, int version) {
        Set<ArticleRecord> articles = new HashSet<>();
        for (int i = 0; i < count; i++) {
            if (random.nextFloat() < 0.01) {
                //skip some values on each run to produce new/deleted records
                continue;
            }
            articles.add(new ArticleRecord(i, version, random.nextInt(10) + 5, random.nextInt(10)));
        }
        dsl.batchInsert(articles).execute();
        dsl.deleteFrom(ARTICLE).where(ARTICLE.VERSION.lessThan(version-1)).execute();
    }

    /**
     * Publish detected changes to RabbitMQ and return count of changed records.
     */
    @Transactional(readOnly = true)
    public long processChanges(int version) {
        final var OLD = ARTICLE.as("OLD");
        final var NEW = ARTICLE.as("NEW");

        final var OLD_ARTICLES = ARTICLE.where(ARTICLE.VERSION.eq(version - 1)).as(OLD);
        final var NEW_ARTICLES = ARTICLE.where(ARTICLE.VERSION.eq(version)).as(NEW);

        final var fetch = dsl.select(
                OLD.ARTICLE_ID,
                OLD.PRICE,
                OLD.STOCK,
                NEW.ARTICLE_ID,
                NEW.PRICE,
                NEW.STOCK)
                .from(OLD_ARTICLES).fullOuterJoin(NEW_ARTICLES).using(ARTICLE.ARTICLE_ID)
                .fetchStreamInto(ArticleChange.class);

        return fetch
                .filter(ArticleChange::hasChanged)
                //TODO: Publish changes over RabbitMQ
                //.peek(articleChange -> rabbitTemplate.convertAndSend(queue.getName(), articleChange.toString()))
                .count();
    }

}
